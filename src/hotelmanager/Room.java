package hotelmanager;

import java.math.BigDecimal;

/**
 * This entity class represents a room. Room has its number, price per night 
 * and capacity.
 * 
 * @author Petr Vacek, Kryštof Zvolánek
 */
public class Room {
    Long roomId;
    int capacity;
    BigDecimal pricePerNight;
    int roomNumber;   
    
    public Room() {}
    
    public Room(int capacity, BigDecimal pricePerNight, int roomNumber)
    {
        this.roomId = null;
        this.capacity = capacity;
        this.pricePerNight = pricePerNight;
        this.roomNumber = roomNumber;
    }

    public Long getRoomId() 
    {
        return roomId;
    }

    public void setId(Long roomId) 
    {
        this.roomId = roomId;
    }

    public int getCapacity() 
    {
        return capacity;
    }

    public void setCapacity(int capacity) 
    {
        this.capacity = capacity;
    }

    public BigDecimal getPricePerNight() 
    {
        return pricePerNight;
    }

    public void setPricePerNight(BigDecimal pricePerNight) 
    {
        this.pricePerNight = pricePerNight;
    }

    public int getRoomNumber() 
    {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) 
    {
        this.roomNumber = roomNumber;
    }
    
    @Override
    public String toString() {
        return "Room{" + "id=" + roomId + "} : #" + roomNumber + " $: " 
                + pricePerNight;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Room other = (Room) obj;
        /*
         * if this.id.equals(other.id)
         *    return true;
         * else
         *    return false;
         */
        
        //copy paste ---where the hell is KISS?
        if (this.roomId != other.roomId && (this.roomId == null || !this.roomId.equals(other.roomId))) {
            return false;
        } else {
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.roomId != null ? this.roomId.hashCode() : 0);
        return hash;
    }
}
