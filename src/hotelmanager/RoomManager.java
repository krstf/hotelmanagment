/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelmanager;

import common.DatabaseException;
import java.util.List;

/**
 * This is an interace for customer manager. It declares what functions are necessary
 * for managements of rooms in a database.
 * 
 * @author Petr Vacek, Kryštof Zvolánek
 */
public interface RoomManager {
    
    /**
     * Stores new room into database. Id for the new rent is automatically
     * generated and stored into roomId attribute.
     * 
     * @param room room to be created.
     * @throws IllegalArgumentException when room is null, or if it has already 
     * assigned roomId.
     * @throws  DatabaseException when database operation fails.
     */
    public void createRoom(Room room) throws DatabaseException;
    
    /**
     * Deletes room from database. 
     * 
     * @param room room to be deleted from database.
     * @throws IllegalArgumentException when room is null, or it has null roomId.
     * @throws DatabaseException when database operation fails.
     */
    public void deleteRoom(Room room) throws DatabaseException;
    
    /**
     * Returns list of all room in database.
     * 
     * @return list of all rooms in database.
     * @throws  DatabaseException when database operation fails.
     */
    public List<Room> getAllRooms() throws DatabaseException;
    
    /**
     * Returns list of all empty room in database.
     * 
     * @return list of all empty rooms in database.
     * @throws  DatabaseException when database operation fails.
     */
   // public List<Room> getEmptyRooms() throws DatabaseException;
    
    /**
     * Returns room with given roomId.
     * 
     * @param roomId primary key of requested room.
     * @return rent with given roomId or null if no such room exists.
     * @throws IllegalArgumentException when given roomId is null.
     * @throws  DatabaseException when database operation fails.
     */
    public Room getRoom(Long roomId) throws DatabaseException;
    
    /**
     * Updates room in database.
     * 
     * @param room updated room to be stored into database.
     * @throws IllegalArgumentException when room is null, or it has null roomId.
     * @throws  DatabaseException when database operation fails.
     */
    public void updateRoom(Room room) throws DatabaseException;  
}
