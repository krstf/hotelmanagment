/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelmanager;

import common.ServiceFailureException;
import common.DBUtils;
import common.DatabaseException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author Petr Vacek, Kryštof Zvolánek
 */
public class RentManagerImpl implements RentManager
{
    
   public static final Logger logger = Logger.getLogger(CustomerManagerImpl.class.getName());
    
    private DataSource dataSource;
    
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    private void checkDataSource() {
        if (dataSource == null) {
            throw new IllegalStateException("DataSource is not set");
        }
    }
    
    void validate(Rent rent) {
        if (rent == null)
            throw new IllegalArgumentException("rent is null");
        if (rent.getStartDate() == null)
            throw new IllegalArgumentException("rent start date unset");
        if (rent.getEndDate() != null && rent.getEndDate().before(rent.getStartDate()))
            throw new IllegalArgumentException("rent start date before end date");
        hotelmanager.CustomerManagerImpl.validate(rent.getCustomer());
        hotelmanager.RoomManagerImpl.validate(rent.getRoom());
        
        CustomerManager cm = new CustomerManagerImpl();
        if (rent.getCustomer().getId() == null || cm.getCustomer(rent.getCustomer().getId()) == null)
            throw new IllegalArgumentException("customer is not in database");
        
        RoomManager rm = new RoomManagerImpl();
        if (rent.getRoom().getRoomId() == null || rm.getRoom(rent.getRoom().getRoomId()) == null)
            throw new IllegalArgumentException("room is not in database");
        
        
        
    }
    
    @Override
    public void createRent(Rent rent) throws DatabaseException {
        checkDataSource();
        
        if (rent.getId() != null) {
            throw new IllegalArgumentException("rent id is already set");            
        }
        
        validate(rent);
        
        List<Rent> rents = getAllRents();
        for (Rent r : rents)
        {
            /*if (r.getCustomer() == rent.getCustomer())
                throw new IllegalArgumentException("customer is already in a different room");*/
            if (r.getRoom() == rent.getRoom())
                throw new IllegalArgumentException("room is already taken");
        }
        
        
        
        
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            // Temporary turn autocommit mode off. It is turned back on in 
            // method DBUtils.closeQuietly(...) 
            conn.setAutoCommit(false);
            
            st = conn.prepareStatement(
                    "INSERT INTO RENT (roomid,customerid,start,end) VALUES (?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            st.setLong(1, rent.getRoom().getRoomId());
            st.setLong(2, rent.getCustomer().getId());
            st.setDate(3, new java.sql.Date(rent.getStartDate().getTime()));
            st.setDate(4, new java.sql.Date(rent.getEndDate().getTime()));
            int addedRows = st.executeUpdate();
            
            DBUtils.checkUpdatesCount(addedRows, rent, true);         
            
            Long id = DBUtils.getId(st.getGeneratedKeys());
            rent.setId(id);
            conn.commit();
 
        } catch (SQLException ex) {
            String msg = "Error when inserting rent " + rent;
            logger.log(Level.SEVERE, msg, ex);
            throw new DatabaseException(msg, ex);
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        } 
    }

    @Override
    public void deleteRent(Rent rent) throws DatabaseException {
        if (rent == null) {
            throw new IllegalArgumentException("rent is null");
        }
        if (rent.getId() == null) {
            throw new IllegalArgumentException("rent id is null");
        }
        
        checkDataSource();
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            // Temporary turn autocommit mode off. It is turned back on in 
            // method DBUtils.closeQuietly(...) 
            conn.setAutoCommit(false);
            st = conn.prepareStatement(
                    "DELETE FROM rent WHERE id = ?");
            st.setLong(1, rent.getId());
            int diff = st.executeUpdate();
            
            if (diff == 0)
                throw new IllegalArgumentException("rent not found");
            if (diff != 1)
                throw new DatabaseException("multiple rents deleted - panic!");
            conn.commit();
        } catch (SQLException ex) {
            String msg = "Error when deleting rent " + rent;
            logger.log(Level.SEVERE, msg, ex);
            throw new DatabaseException(msg, ex);
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        } 
    }
    
    Rent resultSetToRent(ResultSet rs) throws SQLException {
        Rent rent = new Rent();
        CustomerManager cm = new CustomerManagerImpl();
        RoomManager rm = new RoomManagerImpl();

        
        rent.setId(rs.getLong("id"));
        rent.setCustomer(cm.getCustomer(rs.getLong("customerid")));
        rent.setRoom(rm.getRoom(rs.getLong("roomid")));
        rent.setStartDate(rs.getDate("start"));
        rent.setEndDate(rs.getDate("end"));
        return rent;
    }
    
    @Override
    public List<Rent> getAllRents() throws DatabaseException
    {
        checkDataSource();
        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement(
                    "SELECT id,roomid,customerid,start, end FROM rent");
            ResultSet rs = st.executeQuery();
            
            List<Rent> result = new ArrayList<>();
            while (rs.next()) {
                result.add(resultSetToRent(rs));
            }
            return result;
            
        } catch (SQLException ex) {
            throw new DatabaseException(
                    "Error when retrieving all rents", ex);
        } finally {
            DBUtils.closeQuietly(conn, st);
        } 
    }

    @Override
    public Rent getRent(Long id) throws DatabaseException {
        if (id == null) {
            throw new IllegalArgumentException("Rent id is null");
        }
        
        checkDataSource();
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement(
                    "\"SELECT id,roomid,customerid,start, end FROM rent WHERE id = ?");
            st.setLong(1, id);
            ResultSet rs = st.executeQuery();
            
            if (rs.next()) {
                Rent rent = resultSetToRent(rs);

                if (rs.next()) {
                    throw new DatabaseException(
                            "Internal error: More entities with the same id found "
                            + "(source id: " + id + ", found " + rent + " and " + resultSetToRent(rs));                    
                }            
                
                return rent;
            } else {
                return null;
            }
            
        } catch (SQLException ex) {
            String msg = "Error when retrieving rent with id " + id;
            logger.log(Level.SEVERE, msg, ex);
            throw new DatabaseException(msg, ex);
        } finally {
            DBUtils.closeQuietly(conn, st);
        }
    }

    @Override
    public void updateRent(Rent rent) throws DatabaseException {
        validate(rent);
        if (rent.getId() == null) {
            throw new IllegalArgumentException("rent id is null");            
        }
        
        checkDataSource();
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            // Temporary turn autocommit mode off. It is turned back on in 
            // method DBUtils.closeQuietly(...) 
            conn.setAutoCommit(false);
            st = conn.prepareStatement(
                    "UPDATE customer SET roomid = ?, customerid = ?, start = ?, end = ? WHERE id = ?");                            
            st.setLong(1, rent.getRoom().getRoomId());
            st.setLong(2, rent.getCustomer().getId());
            st.setDate(3, new java.sql.Date(rent.getStartDate().getTime()));
            st.setDate(4, new java.sql.Date(rent.getEndDate().getTime()));
            st.setLong(5, rent.getId());
            
            int changed = st.executeUpdate();
            if (changed == 0)
                throw new IllegalArgumentException("Entity " + rent + " does not exist in the database");
            if (changed != 1) {
                throw new DatabaseException("Internal Error: Unexpected number of rows"
                        + "changed when trying to update room " + rent);
            } 
            
            conn.commit();
            } catch (SQLException ex) {
                String msg = "Error when updating rent " + rent;
                logger.log(Level.SEVERE, msg, ex);
                throw new DatabaseException(msg, ex);
            } finally {
                DBUtils.doRollbackQuietly(conn);
                DBUtils.closeQuietly(conn, st);
            }
    }
    /*
    //unnecessary
    void putCustomerIntoRoom(Customer c1, Room r3) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
*/

     /**
     * Find all graves that contain no body. 
     * 
     * @return collection of all empty graves
     * @throws ServiceFailureException when db operation fails.
     */
    
    public List<Room> findEmptyRooms() throws ServiceFailureException {
        checkDataSource();        
        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement(
                    "SELECT ROOM.id, CAPACITY, PRICEPERNIGHT, ROOMNUMBER " +
                    "FROM ROOM LEFT JOIN CUSTOMER ON ROOM.id = CUSTOMER.id " +
                    "GROUP BY ROOM.id, CAPACITY, PRICEPERNIGHT, ROOMNUMBER " +
                    "HAVING COUNT(CUSTOMER.id) = 0");
            return RoomManagerImpl.executeQueryForMultipleRooms(st);
        } catch (SQLException ex) {
            String msg = "Error when trying to find empty graves";
            logger.log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        } finally {
            DBUtils.closeQuietly(conn, st);
        }
    }
    
    
    public Room findRoomWithCustomer(Customer customer) {
        List<Rent> rents = getAllRents();
        for (Rent r : rents)
        {
            if (r.getCustomer().equals(customer))
                return r.getRoom();
        }
        return null;
    }
    
      
    /*
    //unnecessary
    void removeCustomerFromRoom(Customer c3, Room r3) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    */
}
