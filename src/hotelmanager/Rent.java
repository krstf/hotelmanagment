package hotelmanager;

import java.util.Date;

/**
 * This entity class represents rent of a room. Rent has start day, end day 
 * and represents relationship between a customer and a room.
 * 
 * @author Petr Vacek, Kryštof Zvolánek
 */
public class Rent {
    Long id;
    Date startDate;
    Date endDate;
    Room room;
    Customer customer;

    public Rent(Date startDate, Date endDate, Room room, Customer customer) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.room = room;
        this.customer = customer;
    }
    
    
    public Rent() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = 89L;
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
