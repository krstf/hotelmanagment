package hotelmanager;

import java.util.Date;

/**
 * This entity class represents a Customer. Customer has gender, name, surname and birthday.
 * 
 * @author Petr Vacek, Kryštof Zvolánek
 */
public class Customer {
    Long id;
    String name;
    Date birthday;
    Gender gender;
    
    public Customer()
    {
    }
    
    public Customer(String name, Date birthday, Gender gender)
    {
        this.name = name;
        this.birthday = birthday;
        this.gender = gender;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
    
    @Override
    public String toString() {
        return "Customer{" + "id=" + id + "} : " + name;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Customer other = (Customer) obj;
        /*
         * if this.id.equals(other.id)
         *    return true;
         * else
         *    return false;
         */
        
        //copy paste ---where the hell is KISS?
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        } else {
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
    
    
    
}
