/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelmanager;

import common.DatabaseException;
import common.ServiceFailureException;
import java.util.List;

/**
 * This is an interace for rent manager. It declares what functions are necessary
 * for managements of rents in a database.
 * 
 * @author Petr Vacek, Kryštof Zvolánek
 */
public interface RentManager 
{
    /**
     * Stores new rent into database. Id for the new rent is automatically
     * generated and stored into id attribute.
     * 
     * @param rent rent to be created.
     * @throws IllegalArgumentException when rent is null, or if it has already 
     * assigned id.
     * @throws  DatabaseException when database operation fails.
     */
    public void createRent(Rent rent) throws DatabaseException;
    
    /**
     * Deletes rent from database. 
     * 
     * @param rent rent to be deleted from database.
     * @throws IllegalArgumentException when rent is null, or it has null id.
     * @throws DatabaseException when database operation fails.
     */
    public void deleteRent(Rent rent)  throws DatabaseException;
    
    /**
     * Returns list of all rents in database.
     * 
     * @return list of all rents in database.
     * @throws  DatabaseException when database operation fails.
     */
    public List<Rent> getAllRents() throws DatabaseException;
    
      /**
     * Find all graves that contain no body. 
     * 
     * @return collection of all empty graves
     * @throws ServiceFailureException when db operation fails.
     */
    
     public List<Room> findEmptyRooms() throws ServiceFailureException;
    
    
    /**
     * Returns rent with given id.
     * 
     * @param id primary key of requested rent.
     * @return rent with given id or null if no such rent exists.
     * @throws IllegalArgumentException when given id is null.
     * @throws  DatabaseException when database operation fails.
     */
    public Rent getRent(Long id) throws DatabaseException;
    
    /**
     * Updates rent in database.
     * 
     * @param rent updated customer to be stored into database.
     * @throws IllegalArgumentException when rent is null, or it has null id.
     * @throws  DatabaseException when database operation fails.
     */
    public void updateRent(Rent rent) throws DatabaseException;
}
