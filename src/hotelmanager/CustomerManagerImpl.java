/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelmanager;

import common.DBUtils;
import common.DatabaseException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.sql.DataSource;

/**
 *
 * @author Petr Vacek, Kryštof Zvolánek
 */
public class CustomerManagerImpl implements CustomerManager
{
    public static final Logger logger = Logger.getLogger(CustomerManagerImpl.class.getName());
    
    private DataSource dataSource;
    
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    private void checkDataSource() {
        if (dataSource == null) {
            throw new IllegalStateException("DataSource is not set");
        }
    }
    
    static boolean validName(String name) {
    if (name == null)
        return false;
    if (name.trim().isEmpty())
        return false;
    String regx = "^[\\p{L} .'-]+$";
    Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
    Matcher matcher = pattern.matcher(name);
    return matcher.find();
}
    
    static void validateBirthday(Calendar birthday)
    {
        Calendar today = Calendar.getInstance();  
        int age = today.get(Calendar.YEAR) - birthday.get(Calendar.YEAR);  
        if (today.get(Calendar.MONTH) < birthday.get(Calendar.MONTH)) {
        age--;  
        } else if (today.get(Calendar.MONTH) == birthday.get(Calendar.MONTH)
            && today.get(Calendar.DAY_OF_MONTH) < birthday.get(Calendar.DAY_OF_MONTH)) {
        age--;  
        }
        if ((age < 18)||(age > 130))
            throw new IllegalArgumentException("invalid customer birthday");
    }
    
    public static void validate(Customer customer) {
        if (customer == null)
            throw new IllegalArgumentException("customer is null");
        if (!validName(customer.getName()))
            throw new IllegalArgumentException("invalid customer name");
        
        if (customer.getBirthday() == null)
            throw new IllegalArgumentException("invalid customer birthday");
        
        
        Calendar customerBirthday = new GregorianCalendar();
        customerBirthday.setTime(customer.getBirthday());
        
        validateBirthday(customerBirthday);
        if (customer.getGender() == null)
            throw new IllegalArgumentException("customer gender not set");
    }
    
    Long getKey(ResultSet keyRS, Room room) throws DatabaseException, SQLException {
        if (keyRS.next()) {
            if (keyRS.getMetaData().getColumnCount() != 1) {
                throw new DatabaseException("Internal Error: Generated key"
                        + "retriving failed when trying to insert room " + room
                        + " - wrong key fields count: " + keyRS.getMetaData().getColumnCount());
            }
            Long result = keyRS.getLong(1);
            if (keyRS.next()) {
                throw new DatabaseException("Internal Error: Generated key"
                        + "retriving failed when trying to insert room " + room
                        + " - more keys found");
            }
            return result;
        } else {
            throw new DatabaseException("Internal Error: Generated key"
                    + "retriving failed when trying to insert room " + room
                    + " - no key found");
        }
    }
    
    Customer resultSetToCustomer(ResultSet rs) throws SQLException {
        Customer customer = new Customer();
        customer.setId(rs.getLong("id"));
        customer.setName(rs.getString("name"));
        customer.setBirthday(rs.getDate("birthday"));
        customer.setGender(Gender.valueOf(rs.getString("gender")));
        return customer;
    }
    
    
    @Override
    public void createCustomer(Customer customer) throws DatabaseException {
        checkDataSource();
        validate(customer);
        if (customer.getId() != null) {
            throw new IllegalArgumentException("customer id is already set");            
        }
        
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            // Temporary turn autocommit mode off. It is turned back on in 
            // method DBUtils.closeQuietly(...) 
            conn.setAutoCommit(false);
            
            st = conn.prepareStatement(
                    "INSERT INTO CUSTOMER (name,birthday,gender) VALUES (?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            st.setString(1, customer.getName());
            st.setDate(2, new java.sql.Date(customer.getBirthday().getTime()));
            st.setString(3, customer.getGender().name());
            int addedRows = st.executeUpdate();
            
            DBUtils.checkUpdatesCount(addedRows, customer, true);         
            
            Long id = DBUtils.getId(st.getGeneratedKeys());
            customer.setId(id);
            conn.commit();
            
            
        } catch (SQLException ex) {
            String msg = "Error when inserting customer " + customer;
            logger.log(Level.SEVERE, msg, ex);
            throw new DatabaseException(msg, ex);
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }
    }

    @Override
    public void deleteCustomer(Customer customer) throws DatabaseException {
        
        if (customer == null) {
            throw new IllegalArgumentException("Customer is null");
        }
        if (customer.getId() == null) {
            throw new IllegalArgumentException("Customer id is null");
        }
        
        checkDataSource();
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            // Temporary turn autocommit mode off. It is turned back on in 
            // method DBUtils.closeQuietly(...) 
            conn.setAutoCommit(false);
            st = conn.prepareStatement(
                    "DELETE FROM customer WHERE id = ?");
            st.setLong(1, customer.getId());
            int diff = st.executeUpdate();
            
            if (diff == 0)
                throw new IllegalArgumentException("customer not found");
            if (diff != 1)
                throw new DatabaseException("multiple customers deleted - panic!");
            conn.commit();
        } catch (SQLException ex) {
            String msg = "Error when deleting customer " + customer;
            logger.log(Level.SEVERE, msg, ex);
            throw new DatabaseException(msg, ex);
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }
    }

    @Override
    public List<Customer> getAllCustomers() throws DatabaseException {
        checkDataSource();
        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement(
                    "SELECT id,name,birthday,gender FROM customer");
            ResultSet rs = st.executeQuery();
            
            List<Customer> result = new ArrayList<>();
            while (rs.next()) {
                result.add(resultSetToCustomer(rs));
            }
            return result;
            
        } catch (SQLException ex) {
            throw new DatabaseException(
                    "Error when retrieving all customer", ex);
        } finally {
            DBUtils.closeQuietly(conn, st);
        }
    }

    @Override
    public Customer getCustomer(Long id) throws DatabaseException {
        if (id == null) {
            throw new IllegalArgumentException("Customer id is null");
        }
        
        checkDataSource();
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement(
                    "SELECT id,name,birthday,gender FROM customer WHERE id = ?");
            st.setLong(1, id);
            ResultSet rs = st.executeQuery();
            
            if (rs.next()) {
                Customer customer = resultSetToCustomer(rs);

                if (rs.next()) {
                    throw new DatabaseException(
                            "Internal error: More entities with the same id found "
                            + "(source id: " + id + ", found " + customer + " and " + resultSetToCustomer(rs));                    
                }            
                
                return customer;
            } else {
                return null;
            }
            
        } catch (SQLException ex) {
            String msg = "Error when retrieving customer with id " + id;
            logger.log(Level.SEVERE, msg, ex);
            throw new DatabaseException(msg, ex);
        } finally {
            DBUtils.closeQuietly(conn, st);
        }
    }

    @Override
    public void updateCustomer(Customer customer) throws DatabaseException {
        validate(customer);
        if (customer.getId() == null) {
            throw new IllegalArgumentException("customer id is null");            
        }
        
        checkDataSource();
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            // Temporary turn autocommit mode off. It is turned back on in 
            // method DBUtils.closeQuietly(...) 
            conn.setAutoCommit(false);
            st = conn.prepareStatement(
                    "UPDATE customer SET name = ?, birthday = ?, gender = ? WHERE id = ?");                            
            st.setString(1, customer.getName());
            st.setDate(2, new java.sql.Date(customer.getBirthday().getTime()));
            st.setString(3, customer.getGender().name());
            st.setLong(4, customer.getId());
            
            int changed = st.executeUpdate();
            if (changed == 0)
                throw new IllegalArgumentException("Entity " + customer + " does not exist in the database");
            if (changed != 1) {
                throw new DatabaseException("Internal Error: Unexpected number of rows"
                        + "changed when trying to update room " + customer);
            } 
            
            conn.commit();
            } catch (SQLException ex) {
                String msg = "Error when updating customer " + customer;
                logger.log(Level.SEVERE, msg, ex);
                throw new DatabaseException(msg, ex);
            } finally {
                DBUtils.doRollbackQuietly(conn);
                DBUtils.closeQuietly(conn, st);
            }
    }
    
}
