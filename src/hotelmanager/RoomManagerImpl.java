/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelmanager;

import common.DBUtils;
import common.DatabaseException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author Petr Vacek, Kryštof Zvolánek
 */
public class RoomManagerImpl implements RoomManager
{
    public static final Logger logger = Logger.getLogger(RoomManagerImpl.class.getName());
    
    private DataSource dataSource;
    
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    private void checkDataSource() {
        if (dataSource == null) {
            throw new IllegalStateException("DataSource is not set");
        }
    }
    
    public static void validate(Room room) {
        if (room == null) {
            throw new IllegalArgumentException("room is null");            
        }
        if (room.getRoomNumber() <= 0) {
            throw new IllegalArgumentException("room number is not positive");            
        }
        if (room.getCapacity() <= 0) {
            throw new IllegalArgumentException("room capacity is not positive");            
        }
        if (room.getPricePerNight().signum()==-1) {
            throw new IllegalArgumentException("room price per night is negative");            
        }    
    }
    
    Long getKey(ResultSet keyRS, Room room) throws DatabaseException, SQLException {
        if (keyRS.next()) {
            if (keyRS.getMetaData().getColumnCount() != 1) {
                throw new DatabaseException("Internal Error: Generated key"
                        + "retriving failed when trying to insert room " + room
                        + " - wrong key fields count: " + keyRS.getMetaData().getColumnCount());
            }
            Long result = keyRS.getLong(1);
            if (keyRS.next()) {
                throw new DatabaseException("Internal Error: Generated key"
                        + "retriving failed when trying to insert room " + room
                        + " - more keys found");
            }
            return result;
        } else {
            throw new DatabaseException("Internal Error: Generated key"
                    + "retriving failed when trying to insert room " + room
                    + " - no key found");
        }
    }
    
    @Override
    public void createRoom(Room room) throws DatabaseException {
        checkDataSource();
        validate(room);
        if (room.getRoomId() != null) {
            throw new IllegalArgumentException("room id is already set");            
        }
        
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            // Temporary turn autocommit mode off. It is turned back on in 
            // method DBUtils.closeQuietly(...) 
            conn.setAutoCommit(false);
            
            st = conn.prepareStatement(
                    "INSERT INTO ROOM (capacity,pricePerNight,roomNumber) VALUES (?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            st.setInt(1, room.getCapacity());
            st.setBigDecimal(2, room.getPricePerNight());
            st.setInt(3, room.getRoomNumber());
            int addedRows = st.executeUpdate();
            
            DBUtils.checkUpdatesCount(addedRows, room, true);         
            
            Long id = DBUtils.getId(st.getGeneratedKeys());
            room.setId(id);
            conn.commit();
            
            
        } catch (SQLException ex) {
            String msg = "Error when inserting room " + room;
            logger.log(Level.SEVERE, msg, ex);
            throw new DatabaseException(msg, ex);
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }
    }

    @Override
    public void deleteRoom(Room room) throws DatabaseException 
    {
        if (room == null) {
            throw new IllegalArgumentException("Room is null");
        }
        if (room.getRoomId() == null) {
            throw new IllegalArgumentException("Room id is null");
        }
        
        checkDataSource();
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            // Temporary turn autocommit mode off. It is turned back on in 
            // method DBUtils.closeQuietly(...) 
            conn.setAutoCommit(false);
            st = conn.prepareStatement(
                    "DELETE FROM room WHERE id = ?");
            st.setLong(1, room.getRoomId());
            int diff = st.executeUpdate();
            
            if (diff == 0)
                throw new IllegalArgumentException("room not found");
            if (diff != 1)
                throw new DatabaseException("multiple rooms deleted - panic!");
            conn.commit();
        } catch (SQLException ex) {
            String msg = "Error when deleting room " + room;
            logger.log(Level.SEVERE, msg, ex);
            throw new DatabaseException(msg, ex);
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }
    }

    @Override
    public List<Room> getAllRooms() throws DatabaseException {
        checkDataSource();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement(
                    "SELECT id,capacity,pricePerNight,roomNumber FROM room");
            rs = st.executeQuery();

            List<Room> result = new ArrayList<>();
            while (rs.next()) {
                result.add(resultSetToRoom(rs));
            }
            return result;

        } catch (SQLException ex) {
            throw new DatabaseException(
                    "Error when retrieving all rooms", ex);
        } finally {
            DBUtils.closeQuietly(conn, st);
        }
    }

    static List<Room> executeQueryForMultipleRooms(PreparedStatement st) throws SQLException {
        ResultSet rs = st.executeQuery();
        List<Room> result = new ArrayList<Room>();
        while (rs.next()) {
            result.add(rsToRoom(rs));
        }
        return result;
    }
    
    private static Room rsToRoom(ResultSet rs) throws SQLException {
        Room result = new Room();
        result.setId(rs.getLong("ID"));
        result.setCapacity(rs.getInt("CAPACITY"));
        result.setPricePerNight(rs.getBigDecimal("PRICEPERNIGHT"));
        result.setRoomNumber(rs.getInt("ROOMNUMBER"));
        return result;
    }

    
    
    
    Room resultSetToRoom(ResultSet rs) throws SQLException {
        Room room = new Room();
        room.setId(rs.getLong("id"));
        room.setCapacity(rs.getInt("capacity"));
        room.setPricePerNight(rs.getBigDecimal("pricePerNight"));
        room.setRoomNumber(rs.getInt("roomNumber"));
        return room;
    }
    
    @Override
    public Room getRoom(Long roomId) throws DatabaseException
    {
        if (roomId == null) {
            throw new IllegalArgumentException("Room id is null");
        }
        
        checkDataSource();
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement(
                    "SELECT id,capacity,pricePerNight,roomNumber FROM room WHERE id = ?");
            st.setLong(1, roomId);
            ResultSet rs = st.executeQuery();
            
            if (rs.next()) {
                Room room = resultSetToRoom(rs);

                if (rs.next()) {
                    throw new DatabaseException(
                            "Internal error: More entities with the same id found "
                            + "(source id: " + roomId + ", found " + room + " and " + resultSetToRoom(rs));                    
                }            
                
                return room;
            } else {
                return null;
            }
            
        } catch (SQLException ex) {
            String msg = "Error when retrieving room with id " + roomId;
            logger.log(Level.SEVERE, msg, ex);
            throw new DatabaseException(msg, ex);
        } finally {
            DBUtils.closeQuietly(conn, st);
        }
    }

    @Override
    public void updateRoom(Room room) throws DatabaseException 
    {    
        validate(room);
        if (room.getRoomId() == null) {
            throw new IllegalArgumentException("Room id is null");            
        }
        
        checkDataSource();
        Connection conn = null;
        PreparedStatement st = null;
        
        try {
            conn = dataSource.getConnection();
            // Temporary turn autocommit mode off. It is turned back on in 
            // method DBUtils.closeQuietly(...) 
            conn.setAutoCommit(false);
            st = conn.prepareStatement(
                    "UPDATE room SET capacity = ?, pricePerNight = ?, roomNumber = ? WHERE id = ?");                            
            st.setInt(1, room.getCapacity());
            st.setBigDecimal(2, room.getPricePerNight());
            st.setInt(3, room.getRoomNumber());
            st.setLong(4, room.getRoomId());
            
            
            int changed = st.executeUpdate();
            if (changed == 0)
                throw new DatabaseException("Entity " + room + " does not exist in the database");
            if (changed != 1) {
                throw new DatabaseException("Internal Error: Unexpected number of rows"
                        + "changed when trying to update room " + room);
            } 
            
            conn.commit();
            } catch (SQLException ex) {
                String msg = "Error when updating room " + room;
                logger.log(Level.SEVERE, msg, ex);
                throw new DatabaseException(msg, ex);
            } finally {
                DBUtils.doRollbackQuietly(conn);
                DBUtils.closeQuietly(conn, st);
            }
    }
    
}
