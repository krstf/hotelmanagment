package hotelmanager;

/**
 * This exception indicates failure in database.
 *  
 * @author Petr Vacek, Kryštof Zvolánek
 */
public class DatabaseException extends RuntimeException{
    public DatabaseException(String report)
    {
        super(report);
    }
    
    public DatabaseException(Throwable cause)
    {
        super(cause);
    }
    
    public DatabaseException(String report, Throwable cause)
    {
        super(report,cause);
    }    
}
