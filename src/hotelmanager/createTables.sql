CREATE TABLE ROOM (
    "ID" BIGINT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    "CAPACITY" INTEGER NOT NULL,
    "PRICEPERNIGHT" DECIMAL NOT NULL,
    "ROOMNUMBER" INTEGER
);

CREATE TABLE "CUSTOMER" (
    "ID" BIGINT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    "NAME" VARCHAR(255),
    "BIRTHDAY" DATE,
    "GENDER" VARCHAR(255)
);

CREATE TABLE "RENT" (
    "ID" BIGINT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    "ROOMID" BIGINT REFERENCES ROOM (ID),
    "CUSTOMERID" BIGINT REFERENCES CUSTOMER (ID),
    "START" DATE,
    "END" DATE
);