package hotelmanager;

import common.DatabaseException;
import java.util.List;

/**
 * This is an interace for customer manager. It declares what functions are necessary
 * for managements of customers in a database.
 * 
 * @author Petr Vacek, Kryštof Zvolánek
 */
public interface CustomerManager {
    /**
     * Stores new customer into database. Id for the new grave is automatically
     * generated and stored into id attribute.
     * 
     * @param customer customer to be created.
     * @throws IllegalArgumentException when customer is null, or if it has already 
     * assigned id.
     * @throws  DatabaseException when database operation fails.
     */
    public void createCustomer(Customer customer) throws DatabaseException;
    
    /**
     * Deletes customer from database. 
     * 
     * @param customer customer to be deleted from database.
     * @throws IllegalArgumentException when customer is null, or it has null id.
     * @throws DatabaseException when database operation fails.
     */
    public void deleteCustomer(Customer customer) throws DatabaseException;
    
    /**
     * Returns list of all customers in database.
     * 
     * @return list of all customer in database.
     * @throws  DatabaseException when database operation fails.
     */
    public List<Customer> getAllCustomers() throws DatabaseException;
    
    /**
     * Returns customer with given id.
     * 
     * @param id primary key of requested customer.
     * @return customer with given id or null if no such customer exists.
     * @throws IllegalArgumentException when given id is null.
     * @throws  DatabaseException when database operation fails.
     */
    public Customer getCustomer(Long id) throws DatabaseException;
    
    /**
     * Updates customer in database.
     * 
     * @param customer updated customer to be stored into database.
     * @throws IllegalArgumentException when customer is null, or it has null id.
     * @throws  DatabaseException when database operation fails.
     */
    public void updateCustomer(Customer customer) throws DatabaseException;
}
