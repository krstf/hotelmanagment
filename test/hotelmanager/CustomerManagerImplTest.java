/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelmanager;

import common.DBUtils;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Petr Vacek
 */
public class CustomerManagerImplTest {
    
    private CustomerManagerImpl manager;
    DataSource ds;
    
    private static DataSource prepareDataSource() throws SQLException {
        BasicDataSource ds = new BasicDataSource();
        //we will use in memory database
        ds.setUrl("jdbc:derby:memory:customer;create=true");
        return ds;
    }
    
    @Before
    public void setUp() throws SQLException {
        ds = prepareDataSource();
        DBUtils.executeSqlScript(ds,RoomManager.class.getResource("createTables.sql"));
        manager = new CustomerManagerImpl();
        manager.setDataSource(ds);
    }
    
    @After
    public void tearDown() throws SQLException {
        DBUtils.executeSqlScript(ds,RoomManager.class.getResource("dropTables.sql"));
    }
    

    static void assertCustomerDeepEquals(Customer expected, Customer actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getBirthday(), actual.getBirthday());
        assertEquals(expected.getGender(), actual.getGender());   
    } 
    
    static void assertCustomerCollectionDeepEquals(List<Customer> expected, List<Customer> actual) {
        
        assertEquals(expected.size(), actual.size());
        List<Customer> expectedSortedList = new ArrayList<>(expected);
        List<Customer> actualSortedList = new ArrayList<>(actual);
        Collections.sort(expectedSortedList,customerKeyComparator);
        Collections.sort(actualSortedList,customerKeyComparator);
        for (int i = 0; i < expectedSortedList.size(); i++) {
            assertCustomerDeepEquals(expectedSortedList.get(i), actualSortedList.get(i));
        }
    }
    /**
     * Test of createCustomer method, of class CustomerManagerImpl.
     */
    @Test
    public void testCreateCustomerWithWrongAttributes() {
        
        Calendar calendar = new GregorianCalendar(1973, 9, 13); 
        Date validDate;
        validDate = calendar.getTime();
        
        
        try {
            manager.createCustomer(null);
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }
        
        try {
            manager.createCustomer(new Customer());
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }
        
        try {
            Date invalidDate;
            Calendar invalidYear = new GregorianCalendar(1657, 6, 28);
            invalidDate = invalidYear.getTime();
            manager.createCustomer(new Customer("Kyle Broflovski", invalidDate, Gender.MALE ));
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }
        
        try {
            Date invalidDate;
            Calendar invalidYear = new GregorianCalendar(-1, 6, 28);
            invalidDate = invalidYear.getTime();
            manager.createCustomer(new Customer("Kyle Broflovski", invalidDate, Gender.MALE ));
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }
        
        try {
            manager.createCustomer(new Customer("Kyle [--*--%%$!]", validDate, Gender.SECRET ));
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }
        
        try {
            manager.createCustomer(new Customer("Kyle Broflo132590vski", validDate, Gender.SECRET ));
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }
        
        try {
            manager.createCustomer(new Customer("Kyl366e Broflovski", validDate, Gender.SECRET ));
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }       
        
        try {
            manager.createCustomer(new Customer("[--*--%%$!] Broflovski", validDate, Gender.SECRET ));
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }
        
        try {
            manager.createCustomer(new Customer("       ", validDate, Gender.SECRET ));
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }    
        
        try {
            manager.createCustomer(new Customer(null, validDate, Gender.SECRET ));
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }  
        
        try {
            manager.createCustomer(new Customer("Kyle Broflovski", null, Gender.MALE ));
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }  
        
        try {
            manager.createCustomer(new Customer("Kyle Broflovski", validDate, null ));
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        } 
}

    @Test
    public void createCustomer() {
        Calendar calendar = new GregorianCalendar(1983, 9, 13); 
        Date validDate;
        validDate = calendar.getTime();
        
        Customer kyle = new Customer("Kyle Broflovski", validDate, Gender.MALE );
        manager.createCustomer(kyle);

        Long id = kyle.getId();
        assertNotNull(id);
        Customer result = manager.getCustomer(id);
        assertEquals(kyle, result);
        assertNotSame(kyle, result);
        assertCustomerDeepEquals(kyle, result);
    }
    
    @Test
    public void getCustomer() {
        
        assertNull(manager.getCustomer(1L));
        
        Calendar calendar = new GregorianCalendar(1983, 9, 13); 
        Date validDate;
        validDate = calendar.getTime();
        
        Customer kyle = new Customer("Kyle Broflovski", validDate, Gender.MALE);
        manager.createCustomer(kyle);
        Long id = kyle.getId();

        Customer result = manager.getCustomer(id);
        assertEquals(kyle, result);
        assertCustomerDeepEquals(kyle, result);
    }
    
    @Test
    public void findAllCustomer() {

        assertTrue(manager.getAllCustomers().isEmpty());

        Calendar kyleBDay = new GregorianCalendar(1983, 9, 13); 
        Date kyleDate  = kyleBDay.getTime();
        
        Customer kyle = new Customer("Kyle Broflovski", kyleDate, Gender.MALE);
        
        Calendar kennyBDay = new GregorianCalendar(1984, 3, 27); 
        Date kennyDate = kennyBDay.getTime();
        
        Customer kenny = new Customer("Kenny McCormick", kennyDate, Gender.SECRET);

        manager.createCustomer(kyle);
        manager.createCustomer(kenny);

        List<Customer> expected = Arrays.asList(kyle,kenny);
        List<Customer> actual = manager.getAllCustomers();
                
        assertCustomerCollectionDeepEquals(expected, actual);
    }
    
    /**
     * Test of updateCustomer method, of class CustomerManagerImpl.
     */
    @Test
    public void testUpdateCustomer() {
        Date kyleBirthday;
        Calendar kyleYear = new GregorianCalendar(1995, 2, 1);
        kyleBirthday = kyleYear.getTime();
        
        Date lianeBirthday;
        Calendar lianeYear = new GregorianCalendar(1973, 7, 19);
        lianeBirthday = lianeYear.getTime();
        Customer kyle = new Customer("Kyle Broflovski", kyleBirthday, Gender.MALE);
        Customer liane = new Customer ("Liane Cartman", lianeBirthday, Gender.FEMALE);
        Customer result;
        manager.createCustomer(kyle);
        manager.createCustomer(liane);
        Long kyleId = kyle.getId();
 
        kyle = manager.getCustomer(kyleId);
        kyle.setName("Kenny McCormick");
        manager.updateCustomer(kyle);       
        result = manager.getCustomer(kyleId);
        assertCustomerDeepEquals(kyle, result);
        
        Date kyleChangedBirthday;
        Calendar kyleChangedYear = new GregorianCalendar(1994, 2, 1);
        kyleChangedBirthday = kyleChangedYear.getTime();
        
        kyle = manager.getCustomer(kyleId);
        kyle.setBirthday(kyleChangedBirthday);
        manager.updateCustomer(kyle);       
        result = manager.getCustomer(kyleId);
        assertCustomerDeepEquals(kyle, result);
        
        kyle = manager.getCustomer(kyleId);
        kyle.setGender(Gender.SECRET);
        manager.updateCustomer(kyle); 
        result = manager.getCustomer(kyleId);
        assertCustomerDeepEquals(kyle, result);
        
        Customer databaseLiane = manager.getCustomer(liane.getId());
        assertCustomerDeepEquals(liane, databaseLiane);
    }
    
    @Test
    public void updateCustomerWithWrongAttributes() {

        Calendar calendar = new GregorianCalendar(1983, 9, 13); 
        Date validDate;
        validDate = calendar.getTime();
        
        Customer kyle = new Customer("Kyle Broflovski", validDate, Gender.MALE );
        manager.createCustomer(kyle);

        Long id = kyle.getId();
        
        try {
            manager.updateCustomer(null);
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }
        
        try {
            kyle = manager.getCustomer(id);
            kyle.setId(null);
            manager.updateCustomer(kyle);        
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }

        try {
            kyle = manager.getCustomer(id);
            kyle.setId(id - 1);
            manager.updateCustomer(kyle);        
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }

        try {
            kyle = manager.getCustomer(id);
            kyle.setName(null);
            manager.updateCustomer(kyle);        
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }

        try {
            kyle = manager.getCustomer(id);
            
            Date invalidDate;
            Calendar invalidYear = new GregorianCalendar(1657, 6, 28);
            invalidDate = invalidYear.getTime();
            
            kyle.setBirthday(invalidDate);
            manager.updateCustomer(kyle);        
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }

    }
    
    
    @Test
    public void testDeleteCustomer()
    {
        Date kyleBirthday;
        Calendar kyleYear = new GregorianCalendar(1995, 2, 1);
        kyleBirthday = kyleYear.getTime();
        
        Date kennyBirthday;
        Calendar kennyYear = new GregorianCalendar(1993, 11, 24);
        kennyBirthday = kennyYear.getTime();
        
        
        Customer kyle = new Customer("Kyle Broflovski", kyleBirthday, Gender.SECRET);
        Customer kenny = new Customer("Kenny McCormick", kennyBirthday, Gender.MALE);
        
        manager.createCustomer(kyle);
        manager.createCustomer(kenny);
        
        assertNotNull(manager.getCustomer(kyle.getId()));
        assertNotNull(manager.getCustomer(kenny.getId()));
        int count = manager.getAllCustomers().size();
        
        manager.deleteCustomer(kyle);
        
        assertNull(manager.getCustomer(kyle.getId()));
        assertNotNull(manager.getCustomer(kenny.getId()));
        assertEquals(count-1, manager.getAllCustomers().size());
    }
    
    @Test
    public void deleteCustomerWithWrongAttributes() {
        
        Calendar calendar = new GregorianCalendar(1983, 9, 13); 
        Date validDate;
        validDate = calendar.getTime();
        
        Customer kyle = new Customer("Kyle Broflovski", validDate, Gender.MALE );
        manager.createCustomer(kyle);
        
        try {
            manager.deleteCustomer(null);
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }

        try {
            kyle.setId(null);
            manager.deleteCustomer(kyle);
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }

        try {
            kyle.setId(1L);
            manager.deleteCustomer(kyle);
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }        

    }
    
    static Customer newCustomer( String name, Date birthday, Gender gender) {
        Customer customer = new Customer();
        customer.setGender(gender);
        customer.setName(name);
        customer.setBirthday(birthday);
        return customer;
    }
    
    private static Comparator<Customer> customerKeyComparator = new Comparator<Customer>() {

        @Override
        public int compare(Customer o1, Customer o2) {
            Long k1 = o1.getId();
            Long k2 = o2.getId();
            if (k1 == null && k2 == null) {
                return 0;
            } else if (k1 == null && k2 != null) {
                return -1;
            } else if (k1 != null && k2 == null) {
                return 1;
            } else {
                return k1.compareTo(k2);
            }
        }
    };
    
}
