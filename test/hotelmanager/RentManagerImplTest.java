/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hotelmanager;

import common.DBUtils;
import common.IllegalEntityException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.*;
import javax.sql.DataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.apache.commons.dbcp.BasicDataSource;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collections;

//import static hotelmanager.CustomerManagerImplTest.newCustomer;
import static hotelmanager.CustomerManagerImplTest.newCustomer;
import static hotelmanager.RoomManagerImplTest.newRoom;

/**
 *
 * @author Cristobal
 */
public class RentManagerImplTest {
    
    private RentManagerImpl manager;
    private RoomManagerImpl roomManager;
    private CustomerManagerImpl customerManager;
    private DataSource ds;
    private Date testStartDate;
    private Date testEndDate;

    private static DataSource prepareDataSource() throws SQLException {
        BasicDataSource ds = new BasicDataSource();
        //we will use in memory database
        ds.setUrl("jdbc:derby:memory:roommgr-test;create=true");
        //ds.setUrl("jdbc:derby://localhost:1527/test");
        return ds;
    }
    
    
    private Customer c1, c2, c3, c4, c5, customerNullId, customerNotInDB;
    private Room r1, r2, r3, roomNullId, roomNotInDB;
    
    
     private void prepareTestData() {

        r1 = newRoom( 4, new BigDecimal("100"), 12);
        r2 = newRoom( 5,  new BigDecimal("200"), 13);
        r3 = newRoom( 6,  new BigDecimal("300"), 14);
        
        Date testBirthday;
        Calendar testYear = new GregorianCalendar(1970, 1, 11);
        testBirthday = testYear.getTime();
        
        
        Calendar testYear2 = new GregorianCalendar(2014, 1, 3);
        testStartDate = testYear2.getTime();
        
        
        Calendar testYear3 = new GregorianCalendar(2014, 1, 19);
        testEndDate = testYear3.getTime();
       
        
        c1 = newCustomer("Customer 1", testBirthday, Gender.MALE);
        c2 = newCustomer("Customer 1", testBirthday, Gender.MALE);
        c3 = newCustomer("Customer 1", testBirthday, Gender.MALE);
        c4 = newCustomer("Customer 1", testBirthday, Gender.MALE);
        c5 = newCustomer("Customer 1", testBirthday, Gender.MALE);
      
        
        customerManager.createCustomer(c1);
        customerManager.createCustomer(c2);
        customerManager.createCustomer(c3);
        customerManager.createCustomer(c4);
        customerManager.createCustomer(c5);
        
        roomManager.createRoom(r1);
        roomManager.createRoom(r2);
        roomManager.createRoom(r3);

        roomNullId = newRoom( 4, new BigDecimal("500"), 12);
        roomNotInDB = newRoom( 4, new BigDecimal("501"), 12);
        roomNotInDB.setId(r3.getRoomId() + 100);
        customerNullId = newCustomer("Customer 50", testBirthday, Gender.FEMALE);
        customerNotInDB = newCustomer("Customer 51", testBirthday, Gender.FEMALE);
        customerNotInDB.setId(c5.getId() + 101);
       
    }
    
     
    @Before
    public void setUp() throws SQLException {
        ds = prepareDataSource();
        DBUtils.executeSqlScript(ds, RentManager.class.getResource("createTables.sql"));
        manager = new RentManagerImpl();
        manager.setDataSource(ds);
        customerManager = new CustomerManagerImpl();
        customerManager.setDataSource(ds);
        roomManager = new RoomManagerImpl();
        roomManager.setDataSource(ds);
        prepareTestData();
    }
    
    
    
    @After
    public void tearDown() throws SQLException {
        DBUtils.executeSqlScript(ds, RentManager.class.getResource("dropTables.sql"));
    }
    
    
     @Test
    public void findRoomWithCustomer() {
        
        assertNull(manager.findRoomWithCustomer(c1));
        assertNull(manager.findRoomWithCustomer(c2));
        assertNull(manager.findRoomWithCustomer(c3));
        assertNull(manager.findRoomWithCustomer(c4));
        assertNull(manager.findRoomWithCustomer(c5));
        
        Rent rent1 = newRent(testStartDate, testEndDate, r1, c1 );
        manager.createRent(rent1);

        assertEquals(r3, manager.findRoomWithCustomer(c1));
        //assertRoomDeepEquals(r3, manager.findRoomWithCustomer(c1));
        assertNull(manager.findRoomWithCustomer(c2));
        assertNull(manager.findRoomWithCustomer(c3));
        assertNull(manager.findRoomWithCustomer(c4));
        assertNull(manager.findRoomWithCustomer(c5));
        
        try {
            manager.findRoomWithCustomer(null);
            fail();
        } catch (IllegalArgumentException ex) {}
        
        try {
            manager.findRoomWithCustomer(customerNullId);
            fail();
        } catch (IllegalEntityException ex) {}
        
    }
       
    @Test
    public void testCreateRent() {
        
       
        Rent rent1 = newRent(testStartDate, testEndDate, r1, c1 );
        manager.createRent(rent1);
        
        
        Long rentId = rent1.getId();
        assertNotNull(rentId);
        assertEquals(rent1, manager.getRent(rentId));
        assertNotSame(rent1, manager.getRent(rentId));
        
        assertEquals(rent1.getId(), manager.getRent(rentId).getId());
        assertEquals(rent1.getStartDate(), manager.getRent(rentId).getStartDate());
        assertEquals(rent1.getEndDate(), manager.getRent(rentId).getEndDate());
        assertEquals(rent1.getRoom(), manager.getRent(rentId).getRoom());
        assertEquals(rent1.getCustomer(), manager.getRent(rentId).getCustomer());
    
    }
    
        @Test
    public void getRent() {
        
        assertNull(manager.getRent(5L));
        
        Rent rent1 = newRent(testStartDate, testEndDate, r1, c1 );
        manager.createRent(rent1);
        Long rentId = rent1.getId();

        Rent result = manager.getRent(rentId);
        assertEquals(rent1, result);
        assertRentDeepEquals(rent1, result);
    }
    
    
         @Test
    public void updateRent() {
                
        Rent rent1 = newRent(testStartDate, testEndDate, r1, c1 );
        Rent rent2 = newRent(testStartDate, testEndDate, r2, c2 );
        manager.createRent(rent1);
        manager.createRent(rent2);
        Long rentId = rent1.getId();
        Rent result;
        
        rent1 = manager.getRent(rentId);
        rent1.setStartDate(testStartDate);
        manager.updateRent(rent1);        
        result = manager.getRent(rentId);
        assertRentDeepEquals(rent1, result);

        rent1 = manager.getRent(rentId);
        rent1.setEndDate(testEndDate);
        manager.updateRent(rent1);        
        result = manager.getRent(rentId);
        assertRentDeepEquals(rent1, result);

        rent1 = manager.getRent(rentId);
        rent1.setRoom(r3);
        manager.updateRent(rent1);        
        result = manager.getRent(rentId);
        assertRentDeepEquals(rent1, result);

        rent1 = manager.getRent(rentId);
        rent1.setCustomer(c3);
        manager.updateRent(rent1);        
        result = manager.getRent(rentId);
        assertRentDeepEquals(rent1, result);


        // Check if updates didn't affected other records
        assertRentDeepEquals(rent2, manager.getRent(rent2.getId()));
    }
    
    @Test
    public void testDeleteRent() {
        
        Rent rent1 = newRent(testStartDate, testEndDate, r1, c1 );
        Rent rent2 = newRent(testStartDate, testEndDate, r2, c2 );
        Rent rent3 = newRent(testStartDate, testEndDate, r3, c3 );
        
        manager.createRent(rent1);
        manager.createRent(rent2);
        manager.createRent(rent3);
        
        assertNotNull(manager.getRent(r1.getRoomId()));
        assertNotNull(manager.getRent(r2.getRoomId()));
        assertNotNull(manager.getRent(r3.getRoomId()));

        manager.deleteRent(rent1);
        
        assertNull(manager.getRent(r1.getRoomId()));
        assertNotNull(manager.getRent(r2.getRoomId()));
        assertNotNull(manager.getRent(r3.getRoomId()));
        
        manager.deleteRent(rent2);
        
        assertNull(manager.getRent(r1.getRoomId()));
        assertNull(manager.getRent(r2.getRoomId()));
        assertNotNull(manager.getRent(r3.getRoomId()));
        
        manager.deleteRent(rent3);
        
        assertNull(manager.getRent(r1.getRoomId()));
        assertNull(manager.getRent(r2.getRoomId()));
        assertNull(manager.getRent(r3.getRoomId()));
        
    }
    

    
        static void assertRentDeepEquals(Rent expected, Rent actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getStartDate(), actual.getStartDate());
        assertEquals(expected.getEndDate(), actual.getEndDate());
        assertEquals(expected.getRoom(), actual.getRoom());
        assertEquals(expected.getCustomer(), actual.getCustomer());
    }
    
     static Rent newRent( Date start, Date end, Room room, Customer customer) {
        Rent rent = new Rent();
        rent.setStartDate(start);
        rent.setEndDate(end);
        rent.setRoom(room);
        rent.setCustomer(customer);
        return rent;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
