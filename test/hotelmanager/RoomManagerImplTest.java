/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hotelmanager;

import common.DBUtils;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.*;
import javax.sql.DataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.apache.commons.dbcp.BasicDataSource;

import static org.junit.Assert.*;

/**
 *
 * @author Kryštof Zvolánek, Petr Vacek
 */
public class RoomManagerImplTest {
    
    RoomManagerImpl manager;
    DataSource ds;
    
    private static DataSource prepareDataSource() throws SQLException {
        BasicDataSource ds = new BasicDataSource();
        //we will use in memory database
        ds.setUrl("jdbc:derby:memory:room;create=true");
        return ds;
    }
    
    @Before
    public void setUp() throws SQLException {
        ds = prepareDataSource();
        DBUtils.executeSqlScript(ds,RoomManager.class.getResource("createTables.sql"));
        manager = new RoomManagerImpl();
        manager.setDataSource(ds);
    }
    
    
    @After
    public void tearDown() throws SQLException {
        DBUtils.executeSqlScript(ds,RoomManager.class.getResource("dropTables.sql"));
    }

    /**
     * Test of createRoom method, of class RoomManagerImpl.
     * Testing whether the room is created correctly.
     */
    @Test
    public void testCreateRoom() {
        Room room = newRoom(5, new BigDecimal(300),100);
        manager.createRoom(room);
        
        Long roomId = room.getRoomId();
        assertNotNull(roomId);
        assertEquals(room, manager.getRoom(roomId));
        assertNotSame(room, manager.getRoom(roomId));
        
        assertEquals(room.getRoomId(), manager.getRoom(roomId).getRoomId());
        assertEquals(room.getCapacity(), manager.getRoom(roomId).getCapacity());
        assertEquals(room.getRoomNumber(), manager.getRoom(roomId).getRoomNumber());
        assertEquals(room.getPricePerNight(), manager.getRoom(roomId).getPricePerNight());
    
    }

    /**
     * Test of deleteRoom method, of class RoomManagerImpl.
     */
    @Test
    public void testDeleteRoom() {
        Room room = newRoom(5, new BigDecimal(300),100);
        Room room2 = newRoom(3, new BigDecimal(250),101);
        
        manager.createRoom(room);
        manager.createRoom(room2);
                
        assertNotNull(manager.getRoom(room.getRoomId()));
        assertNotNull(manager.getRoom(room2.getRoomId()));
        
        manager.deleteRoom(room);
        
        assertNull(manager.getRoom(room.getRoomId()));
        assertNotNull(manager.getRoom(room2.getRoomId()));
         
               
    }

     @Test
    public void testGetRoom() {
        
        assertNull(manager.getRoom(1L));     // proc to testuji, kde by se mohlo stat ze to vznikne?
        
        Room room = newRoom(5, new BigDecimal(300),100);
        manager.createRoom(room);
        Long roomId = room.getRoomId();
        
        assertEquals(room, manager.getRoom(roomId));
        assertEquals(room.getRoomId(), manager.getRoom(roomId).getRoomId());
        assertEquals(room.getCapacity(), manager.getRoom(roomId).getCapacity());
        assertEquals(room.getRoomNumber(), manager.getRoom(roomId).getRoomNumber());
        assertEquals(room.getPricePerNight(), manager.getRoom(roomId).getPricePerNight());
       
        
    }
    
    /**
     * Test of getAllRooms method, of class RoomManagerImpl.
     */
    @Test
    public void testGetAllRooms() {
        
        assertTrue(manager.getAllRooms().isEmpty() ); //the same question
        
        Room room = newRoom(5, new BigDecimal(300),100);
        Room room2 = newRoom(3, new BigDecimal(250),101);
        
        manager.createRoom(room);
        manager.createRoom(room2);
        
        List<Room> expected = Arrays.asList(room,room2);
        List<Room> actual = manager.getAllRooms();

        Collections.sort(actual,idComparator);
        Collections.sort(expected,idComparator);

        assertEquals(expected, actual);
        
        
    }

    /**
     * Test of getEmptyRooms method, of class RoomManagerImpl.
     */
    @Test
    public void testGetEmptyRooms() {   // bude treba resit pres RENT
        
    }

 

    /**
     * Test of updateRoom method, of class RoomManagerImpl.
     */
    @Test
    public void testUpdateRoom() {
        Room room = newRoom(5, new BigDecimal("300"),100);
       // Room room2 = newRoom(2L,3, new BigDecimal(250),101);
        Room result;
        
        manager.createRoom(room);
     //   manager.createRoom(room2);
        
        Long roomId = room.getRoomId();
        
        
        room = manager.getRoom(roomId);
        room.setCapacity(200);
        manager.updateRoom(room);
        result = manager.getRoom(roomId);
        assertEquals(200, result.getCapacity() );
        assertEquals(new BigDecimal("300"), result.getPricePerNight());
        assertEquals(100, result.getRoomNumber() );
        
        
        room.setPricePerNight(new BigDecimal("200"));
        manager.updateRoom(room);
        result = manager.getRoom(roomId);
        assertEquals(200, result.getCapacity() );
        assertEquals(new BigDecimal("200"), result.getPricePerNight());
        assertEquals(100, result.getRoomNumber() );
        
        room.setRoomNumber(200);
        manager.updateRoom(room);
        result = manager.getRoom(roomId);
        assertEquals(200, result.getCapacity() );
        assertEquals(new BigDecimal("200"), result.getPricePerNight());
        assertEquals(200, result.getRoomNumber());
        
        
        
    }
   
    static Room newRoom(int capacity, BigDecimal pricePerNight, int roomNumber) {
        Room room = new Room();
        room.setCapacity(capacity);
        room.setPricePerNight(pricePerNight);
        room.setRoomNumber(roomNumber);
        return room;
    }
    
     private static Comparator<Room> idComparator = new Comparator<Room>() {

        @Override
        public int compare(Room o1, Room o2) {
            return Long.valueOf(o1.getRoomId()).compareTo(Long.valueOf(o2.getRoomId()));
        }
    };
    
    
}
